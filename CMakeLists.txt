cmake_minimum_required(VERSION 3.14)
project(qt-timer-test)

set(CMAKE_CXX_STANDARD 14)
set(CMAKE_AUTOMOC ON)
set(CMAKE_AUTORCC ON)
set(CMAKE_AUTOUIC ON)


find_package(Qt5 COMPONENTS
        Core
        Widgets
        REQUIRED)

add_executable(qt-timer-test main.cpp)
target_link_libraries(qt-timer-test
        Qt5::Core
        Qt5::Widgets
        )
