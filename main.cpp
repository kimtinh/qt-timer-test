#include <QApplication>
#include <QDebug>
#include <QWidget>
#include <QPainter>
#include <QBasicTimer>

class TestWidget : public QWidget {
public:
    void start();

protected:
    void paintEvent(QPaintEvent *) override;
    void timerEvent(QTimerEvent *) override;

private:
    QBasicTimer timer;
};

void TestWidget::start() {
    timer.start(66,this);
}

void TestWidget::paintEvent(QPaintEvent *) {
    // do nothing
}

void TestWidget::timerEvent(QTimerEvent *) {
    update();
}

int main(int argc, char *argv[]) {
    QApplication a(argc, argv);
    auto *widget = new TestWidget;

    widget->show();
    widget->start();

    return a.exec();
}
